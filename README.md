package pkg191019;

import java.util.Scanner;
/*
*@author W I C H O
orden de numeros mayores con funciones
todas las aplicaciones realizadas hasta ahora pasarlas a funciones
martes 22, mandarla a repositorio antes de ese dia
comentar cada funcion para mejor entendimiento
*/
public class Main {

    public static void main(String[] args) {
       Scanner leer = new Scanner(System.in);
       int n1;
       int x1;
       int x2;
       
        System.out.println("Ingresa primer variable");
        x1 = leer.nextInt ();
        System.out.println("Ingresa segunda variable");
        x2 = leer.nextInt();
        System.out.println("Selecciona un numero");
        System.out.println("El numero 1 es suma");
        System.out.println("El numero 2 es resta");
        System.out.println("El numero 3 es multiplicacion");
        n1 = leer.nextInt();
           /*
           se usa un if para condicionar y pedir/llamar
           a la funcion SUMA con sus respectivas variables:
           */
       if (n1 == 1){
          int r = suma (x1, x2);
           System.out.println(r);
           /*se usa un else if para condicionar y pedir/llamar
           a la funcion RESTA con sus respectivas variables:
           */   
       }else if(n1 == 2){
          int r = resta (x1, x2);
          /*
          se usa un else if para condicionar y pedir/llamar
          a la funcion MULTIPLICACION con sus respectivas variables
          */
       }else if(n1 == 3){
          int r = multiplicacion (x1, x2);
       }
    }
    public static int suma (int x1, int x2){
        //se crea una funcion llamada SUMA que solo se usara para sumar
        Scanner leer = new Scanner(System.in);
       int r = x1 + x2;
        System.out.println(x1+" + "+x2+" = "+r);
       return 0;
    }
    public static int resta (int x1, int x2){
        //se crea la funcion llamada RESTA que se usara para dicha operacion
       int r = x1 - x2;
        System.out.println(x1+" - "+x2+" = "+r);
       return 0;
    }
    public static int multiplicacion (int x1, int x2){
        //se crea la funcion llamada MULTIPLICACION que se usara para dicha operacion
       int r = x1 * x2;
        System.out.println(x1+" * "+x2+" = "+r);
       return 0;
    }
}
